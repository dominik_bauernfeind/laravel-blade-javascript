<?php

namespace Spatie\BladeJavaScript\Transformers;

class StringTransformer implements Transformer
{
    /**
     * @param mixed $value
     *
     * @return bool
     */
    public function canTransform($value)
    {
        return is_string($value);
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public function transform($value)
    {
        return "'{$this->escape($value)}'";
    }

    protected function escape(string $value)
    {
        return str_replace(['\\', "'", "\r", "\n"], ['\\\\', "\'", '\\r', '\\n'], $value);
    }
}
