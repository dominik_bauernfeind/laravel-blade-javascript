<?php

namespace Spatie\BladeJavaScript\Transformers;

interface Transformer
{
    public function canTransform($value);

    public function transform($value);
}
